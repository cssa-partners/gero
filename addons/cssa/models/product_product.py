# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools
from datetime import date


class Product(models.Model):
    _inherit = 'product.template'

    no_update = fields.Boolean('Non aggiornare prezzo e imposta fattura')