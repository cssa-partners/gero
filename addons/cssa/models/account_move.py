# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools


class AccountMove(models.Model):
    _inherit = 'account.move'

    legale = fields.Boolean('Legale')

    payment_method = fields.Selection([
        ('Bonifico bancario', 'Bonifico bancario'),
        ('Riba', 'Riba')
    ],'Metodo di pagamento')


    def setNoProvvigione(self):
        self.ref = 'no_provvigione'

    @api.onchange('payment_method')
    def _onchange_payment_method(self):
        if self.payment_method:
            bank_name = self.company_id.bank_ids[0].bank_id.name
            bank_iban = "IBAN: " + self.company_id.bank_ids[0].bank_id.iban
            bank_bic = "BIC " + self.company_id.bank_ids[0].bank_id.bic
            bank_conto = "Conto: " + self.company_id.bank_ids[0].journal_id[0].bank_acc_number

            if len(self.invoice_line_ids) > 1:
                for line in self.invoice_line_ids:
                    if 'Anticipo' in line.product_id.name:
                        line.price_unit = -line.price_unit
                        line.quantity = 1
                        break

            sale_order = self.env['sale.order'].search([('invoice_ids','=',self._origin.id)])
            self.narration = "Pagamento tramite " + self.payment_method + "\n\n"

            if len(sale_order.installment_lines) > 1:
                i = 0
                for installment in sale_order.installment_lines:
                    i += 1
                    if i == 1:
                        continue
                    self.narration += installment.value_amount + " con scadenza il " + (str(installment.due_date.day)+"/"+str(installment.due_date.month)+"/"+str(installment.due_date.year)) + "\n"

                self.narration += "\n\n"
            self.narration += bank_name + "\n" + bank_iban + "\n" + bank_bic + "\n" + bank_conto


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    def _get_computed_price_unit(self):
        self.ensure_one()

        if not self.product_id or self.product_id.no_update:
            return self.price_unit
        elif self.move_id.is_sale_document(include_receipts=True):
            # Out invoice.
            price_unit = self.product_id.lst_price
        elif self.move_id.is_purchase_document(include_receipts=True):
            # In invoice.
            price_unit = self.product_id.standard_price
        else:
            return self.price_unit

        if self.product_uom_id != self.product_id.uom_id:
            price_unit = self.product_id.uom_id._compute_price(price_unit, self.product_uom_id)

        return price_unit

    def _get_computed_taxes(self):
        self.ensure_one()

        if self.move_id.is_sale_document(include_receipts=True):
            # Out invoice.
            if self.product_id.taxes_id:
                tax_ids = self.product_id.taxes_id.filtered(lambda tax: tax.company_id == self.move_id.company_id)
            elif self.account_id.tax_ids:
                tax_ids = self.account_id.tax_ids
            else:
                tax_ids = self.env['account.tax']
            if not tax_ids and not self.exclude_from_invoice_tab:
                tax_ids = self.move_id.company_id.account_sale_tax_id
        elif self.move_id.is_purchase_document(include_receipts=True):
            # In invoice.
            if self.product_id.no_update:
                tax_ids = self.tax_ids
            elif self.product_id.supplier_taxes_id:
                tax_ids = self.product_id.supplier_taxes_id.filtered(lambda tax: tax.company_id == self.move_id.company_id)
            elif self.account_id.tax_ids:
                tax_ids = self.account_id.tax_ids
            else:
                tax_ids = self.env['account.tax']
            if not tax_ids and not self.exclude_from_invoice_tab:
                tax_ids = self.move_id.company_id.account_purchase_tax_id
        else:
            # Miscellaneous operation.
            tax_ids = self.account_id.tax_ids

        if self.company_id and tax_ids:
            tax_ids = tax_ids.filtered(lambda tax: tax.company_id == self.company_id)

        return tax_ids


