# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CompletedZones(models.Model):
    _name = 'cssa.completed_zones'
    _description = 'Completed Zones'

    opportunity_id = fields.Integer()
    opportunity_name = fields.Char(string="Opportunità")
    opportunity_city = fields.Char(string="Città")
    opportunity_provincia = fields.Char(string="Provincia")
    opportunity_regione = fields.Char(string="Regione")

    def view_zone(self):
        return {
            'res_model': 'crm.lead',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': self.opportunity_id,
            'target': 'current',
            'flags': {
                'mode': 'readonly'
            }
        }