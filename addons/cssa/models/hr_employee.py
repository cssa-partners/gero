# -*- coding: utf-8 -*-

from odoo import models, fields

class Employee(models.Model):
    _inherit = 'hr.employee'

    n_min_appointments = fields.Integer('N° min di appuntamenti')
    soglia_check = fields.Integer('Soglia check')
    variazione_provvigione = fields.Integer('Variazione provvigione')