# -*- coding: utf-8 -*-

from . import res_partner
from . import crm_lead
from . import zones
from . import completed_zones
from . import calendar
from . import cssa
from . import mail_activity
from . import res_users
from . import payslip
from . import crm_activity_report
from . import linea_business
from . import res_company
from . import sale_order
from . import account_payment_term
from . import project_project
from . import mail_mail
from . import res_bank
from . import account_move
from . import hr_employee
from . import mail_thread
from . import product_product