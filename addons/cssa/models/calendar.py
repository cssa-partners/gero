# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import datetime
from datetime import timedelta, date

import threading
import openerp
import openerp.release
import openerp.sql_db
import openerp.tools

class Meeting(models.Model):
    _inherit = 'calendar.event'

    telefono = fields.Char("N° di telefono")
    indirizzo = fields.Char("Indirizzo")
    referente = fields.Char("Referente")

    @api.model
    def create(self, values):
        calendar_event = super(Meeting, self).create(values)

        if calendar_event.opportunity_id.team_id.name == 'Tecnici':
            new_opportunity = self.env['crm.lead'].search([('id','=',calendar_event.opportunity_id.id)])
            new_opportunity.date_last_appointment = calendar_event.start_datetime

        calendar_phone = ""
        calendar_street = ""
        calendar_city = ""
        calendar_provincia = ""
        calendar_zip = ""
        calendar_contact_name = ""

        if calendar_event.opportunity_id:
            calendar_phone = calendar_event.opportunity_id.phone
            calendar_street = calendar_event.opportunity_id.street
            calendar_city = calendar_event.opportunity_id.city
            calendar_provincia = calendar_event.opportunity_id.provincia
            calendar_zip = calendar_event.opportunity_id.zip
            calendar_contact_name = calendar_event.opportunity_id.contact_name
        else:
            calendar_task = self.env['project.task'].search([('id','=',calendar_event.res_id)])
            if calendar_task:
                calendar_project = self.env['project.project'].search([('id','=',calendar_task.project_id.id)])
                calendar_partner = calendar_project.partner_id
            else:
                calendar_partner = self.env['res.partner'].search([('id','=',calendar_event.res_id)])

            calendar_phone = calendar_partner.phone
            calendar_street = calendar_partner.street
            calendar_city = calendar_partner.city
            calendar_provincia = calendar_partner.provincia
            calendar_zip = calendar_partner.zip
            if len(calendar_partner.child_ids) > 0:
                calendar_contact_name = calendar_partner.child_ids[0].name


        if calendar_event.opportunity_id or calendar_partner:
            calendar_event.telefono = calendar_phone
            calendar_event.indirizzo = calendar_street
            calendar_event.indirizzo = calendar_event.indirizzo + ", " + calendar_city
            calendar_event.indirizzo = calendar_event.indirizzo + ", " + calendar_zip
            calendar_event.indirizzo = calendar_event.indirizzo + " (" + calendar_provincia + ")"
            calendar_event.referente = calendar_contact_name

            calendar_event.name += " - " + calendar_city + " (" + calendar_provincia + ")"

        return calendar_event

    def write(self, values):
        current_tecnico = self.opportunity_id.tecnico
        current_user = self.env.user

        if current_tecnico:
            values['user_id'] = current_tecnico.id
            new_partner_id = self.env['res.partner'].search([('email', '=', current_tecnico.email)]).id
            values['partner_ids'] = [new_partner_id]

            # values['duration'] = 1.5
            # start = datetime.datetime.strptime(self.start.strftime(DEFAULT_SERVER_DATETIME_FORMAT), DEFAULT_SERVER_DATETIME_FORMAT)
            # values['stop'] = start + timedelta(hours=1.5)

        super(Meeting, self).write(values)

