# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import datetime
from datetime import timedelta, date

import threading
import openerp
import openerp.release
import openerp.sql_db
import openerp.tools

class Mail(models.Model):
    _inherit = 'mail.message'

    sale_team = fields.Many2one('crm.team')

    def send(self, auto_commit=False, raise_exception=False):
        sending_user = self.env['res.users'].search([('partner_id','=',self.author_id.id)])
        self.sale_team = sending_user.sale_team_id.id
        return super(Mail, self).send()

class MailTemplate(models.Model):
    _inherit = 'mail.template'

    sale_team = fields.Many2one('crm.team')
    company = fields.Many2one('res.company')