# -*- coding: utf-8 -*-

from odoo import models, fields

class User(models.Model):
    _inherit = 'res.users'

    call_count = fields.Integer('Call Count')

    check_count = fields.Integer('Check Count')
    check_count_container = fields.Integer('Check Count')

    go_count = fields.Integer('Go Count')
    go_count_container = fields.Integer('Go Count')

    commission_lines = fields.One2many('commission.list', related="employee_id.slip_ids.commission_lines")
    line_ids = fields.One2many('hr.payslip.line', related="employee_id.slip_ids.line_ids")
    total_payable = fields.Float('hr.payslip', related="employee_id.slip_ids.total_payable")
    total_stock = fields.Float('hr.payslip', related="employee_id.slip_ids.total_stock")
    total_payed = fields.Float('hr.payslip', related="employee_id.slip_ids.total_payed")

    is_partner = fields.Boolean()
