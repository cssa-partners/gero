# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools
from datetime import date


class ProjectTask(models.Model):
    _inherit = 'project.task'

    user_id = fields.Many2one('res.users', string='Tecnico', index=True, tracking=True,
                              default=lambda self: self.env.user, domain="['&',('groups_id.category_id.name','=','Tecnico'),('groups_id.name','=','User')]")

    tutor = fields.Many2one('res.users', string='Tutor', index=True, tracking=True,
                            default=lambda self: self.env.user, domain="['&',('groups_id.category_id.name','=','Tecnico'),('groups_id.name','=','User')]")

class Project(models.Model):
    _inherit = 'project.project'

    user_id = fields.Many2one('res.users', string='Tecnico', index=True, tracking=True,
                            default=lambda self: self.env.user, domain="['&',('groups_id.category_id.name','=','Tecnico'),('groups_id.name','=','User')]")

    tutor = fields.Many2one('res.users', string='Tutor', index=True, tracking=True,
                              default=False, domain="['&',('groups_id.category_id.name','=','Tecnico'),('groups_id.name','=','User')]")

    date_start_job = fields.Date()
    date_end_job = fields.Date()
    project_completed = fields.Boolean()
    advance_paid = fields.Boolean()
    different_advance_deal = fields.Char()

    @api.model
    def create(self, values):
        project = super(Project, self).create(values)

        phase_1 = self.env['project.task.type'].search([('name','=','PIANIFICATI')])
        phase_2 = self.env['project.task.type'].search([('name','=','IN CORSO')])
        phase_3 = self.env['project.task.type'].search([('name','=','COMPLETATI')])

        phase_1_projects = []
        phase_2_projects = []
        phase_3_projects = []

        for p in phase_1.project_ids:
            phase_1_projects.append(p.id)
        for p in phase_2.project_ids:
            phase_2_projects.append(p.id)
        for p in phase_3.project_ids:
            phase_3_projects.append(p.id)

        phase_1_projects.append(project.id)
        phase_2_projects.append(project.id)
        phase_3_projects.append(project.id)

        phase_1.project_ids = phase_1_projects
        phase_2.project_ids = phase_2_projects
        phase_3.project_ids = phase_3_projects

        task = {
            'project_id': project.id,
            'stage_id': phase_1.id,
            'name': 'Generale'
        }
        self.env['project.task'].create(task)
        project.tutor = False

        return project

    @api.onchange('date_start_job')
    def _onchange_date_start_job(self):
        task_1 = self.env['project.task'].search([('project_id','=',self._origin.id)])[0]
        task_1.gantt_start_date = self.date_start_job

    @api.onchange('tutor')
    def _onchange_tutor(self):
        self.add_new_follower(self.tutor)

    @api.onchange('project_completed')
    def _onchange_project_completed(self):
        for opportunity in self.partner_id.opportunity_ids:
            if opportunity.team_id.id == 10:
                if self.project_completed:
                    tag_lavoro_completato = self.env['crm.lead.tag'].search([('name','=','Lavoro Completato')])
                    opportunity.tag_ids = [tag_lavoro_completato.id]
                    self.date_end_job = date.today()
                else:
                    opportunity.tag_ids = False
                break

    def add_new_follower(self, user):
        follower = self.env['mail.followers'].search(['&',('res_id','=',self._origin.id),('partner_id','=',user.partner_id.id)])
        if not follower:
            reg = {
                'res_id': self._origin.id,
                'res_model': 'project.project',
                'partner_id': user.partner_id.id,
            }
            self.env['mail.followers'].create(reg)