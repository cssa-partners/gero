# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Riba(models.Model):
    _name = 'riba'

    company_id = fields.Many2one('res.company')
    partner_id = fields.Many2one('res.partner', 'Cliente')
    value_amount = fields.Char('Importo')
    due_date = fields.Date('Scadenza')
    account_move_id = fields.Integer()
    status = fields.Selection([('da_presentare','DA PRESENTARE'),('insoluta','INSOLUTA'),('sospesa','SOSPESA'),('pagato','PAGATA'),('presentata','PRESENTATA')],'Stato')
    #castelletto = fields.Char('Castelletto')
    invoice_id = fields.Integer()

    def pay_riba(self):
        current_company = self.env['res.company'].search([('id','=',self.company_id.id)])

        max_castelletto = round(current_company.max_castelletto,2)
        castelletto = round(current_company.castelletto,2)
        value_amount = float(self.value_amount.replace('€', ''))

        if (self.status == 'presentata' or self.status == 'sospesa') and max_castelletto >= (castelletto+value_amount):
            #self.castelletto = '€ ' + str(round((castelletto + value_amount),2))
            castelletto += value_amount

            current_company.castelletto = round(castelletto, 2)

            journal_id = self.env['account.journal'].search([('name','=','Ri.Ba')]).id
            vals = {}
            vals['invoice_ids'] = self.env['account.move'].search([('id','=',self.account_move_id)])
            vals['amount'] = value_amount
            vals['payment_method_id'] = 1
            vals['currency_id'] = 1
            vals['payment_type'] = 'inbound'
            vals['partner_id'] = self.partner_id.id
            vals['partner_type'] = 'customer'
            vals['journal_id'] = journal_id
            vals['payment_date'] = self.due_date

            account_payment = self.env['account.payment'].create(vals)
            account_payment.post()

            self.status = 'pagato'


    def set_status(self, status):
        current_company = self.env['res.company'].search([('id','=',self.company_id.id)])
        castelletto = round(current_company.castelletto,2)
        max_castelletto = round(current_company.max_castelletto,2)

        riba_amount = float(self.value_amount.replace('€', ''))
        if (castelletto-riba_amount) >= 0 and status == 'presentata':
            self.status = status
            current_company.castelletto -= riba_amount
            #self.castelletto = '€ ' + str(round((castelletto - riba_amount),2))
        elif max_castelletto >= (castelletto+riba_amount) and status == 'insoluta':
            self.status = status
            current_company.castelletto += riba_amount
        elif status == 'sospesa':
            self.status = status


    def view_invoice(self):
        return {
            'res_model': 'account.move',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': self.invoice_id,
            'target': 'current',
            'flags': {
                'mode': 'readonly'
            }
        }
