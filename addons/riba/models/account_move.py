# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime

class Account(models.Model):
    _inherit = 'account.move'

    def action_set_up_riba(self):
        current_company = self.env['res.company'].search([('id','=',self.user_id.company_id.id)])

        if current_company.castelletto <= current_company.max_castelletto:
            vals = {}
            sale_order = self.env['sale.order'].search([('invoice_ids','=',self.id)])

            counter = 0
            for line in sale_order.installment_lines:
                if counter > 0:
                    vals['due_date'] = line.due_date
                    vals['value_amount'] = line.value_amount
                    vals['account_move_id'] = self.id
                    #vals['castelletto'] = '€ ' + str(self.company_id.castelletto)
                    vals['status'] = 'da_presentare'

                    riba = self.env['riba'].create(vals)
                    riba.company_id = self.company_id
                    riba.partner_id = self.partner_id
                    riba.invoice_id = self.id

                counter += 1

            return True
